LHCb Use case for CVMFS stratum-0
=================================


Reproducer for the LHCb use case

Setup
-----

For example to create an install dir in */tmp/mytest*, where we will create:
 - 5 transactions per day 
 - keep 7 days of data
 - Each transaction creating ~50 MiB of files
 - files are of size 4000 bytes, with a stddev of 200

Run the command below::
    export INSTALLROOT=/tmp/mytest
    lbuc_setup  --transaction_total=50 --mu=4000 --sigma=200 --period=7 --transactions_per_period=5 /tmp/mytest
    
    
Running
-------

lbuc_install installs *one* transaction in the directory ${INSTALLROOT}/per<day>/trans<transaction number>.
This allows wrapping it up with cvmfs transaction management commands, and it should be run until its return
code is 1 like so::
    while lbuc_install ${INSTALLROOT}; do :; done

To run 1 day in installation/gc::

    export INSTALLROOT=/tmp/mytest
    while lbuc_install ${INSTALLROOT}; do :; done    
    lbuc_gc ${INSTALLROOT}
    lbuc_next ${INSTALLROOT}
    
This should produce an output like so::

    $ while lbuc_install ${INSTALLROOT}; do :; done
    INFO:root:install /tmp/mytest date:1
    INFO:root:Generating transaction 1, 50 MiB data for the day per_1
    INFO:root:per_1 creation time: 1.13126802444 s
    INFO:root:install /tmp/mytest date:1
    INFO:root:Generating transaction 2, 50 MiB data for the day per_1
    INFO:root:per_1 creation time: 1.11653900146 s
    INFO:root:install /tmp/mytest date:1
    INFO:root:Generating transaction 3, 50 MiB data for the day per_1
    INFO:root:per_1 creation time: 1.11286711693 s
    INFO:root:install /tmp/mytest date:1
    INFO:root:Generating transaction 4, 50 MiB data for the day per_1
    INFO:root:per_1 creation time: 1.11538219452 s
    INFO:root:install /tmp/mytest date:1
    INFO:root:Generating transaction 5, 50 MiB data for the day per_1
    INFO:root:per_1 creation time: 1.13199496269 s
    INFO:root:install /tmp/mytest date:1
    $ lbuc_gc ${INSTALLROOT}
    INFO:root:gc /tmp/mytest date:1
    $ lbuc_next ${INSTALLROOT}
    INFO:root:next /tmp/mytest current date:1

Once the install area configured, the following one liner should load the stratum-0::

     while true; do while lbuc_install ${INSTALLROOT}; do :; done; lbuc_gc ${INSTALLROOT}; lbuc_next ${INSTALLROOT}; done

Of course cvmfs commands for transaction, and error handling have to be added where needed...

