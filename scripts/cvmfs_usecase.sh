#!/bin/bash

export INSTALLROOT=/cvmfs/lhcbdev-test.cern.ch/benchmarking/
export CVMFS_REPO_NAME=lhcbdev-test.cern.ch
export AUDIT_LOG=$HOME/audit.csv

# timer init
timer=0


logCSV(){
    DATE=`date '+%Y-%m-%d %H:%M:%S'`
    extra=${3:-""} 
    if [ "$1" = "START" ]
    then
        timer=`date +%s`
        line="$DATE;$1;$2;;"
    else
        delta=$(expr `date +%s` - $timer)
        line="$DATE;$1;$2;$delta;$3"
    fi
    echo $line >> $AUDIT_LOG
}

start_transaction(){
    logCSV "START" "Init_transaction"
    cvmfs_server transaction $CVMFS_REPO_NAME
    rc=$?
    if [ $rc -ne 0 ]
	then
	     logCSV "FAILED" "Init_transaction"
             exit 1 
	else
	    logCSV "SUCCESS" "Init_transaction"
	fi
}

publish_transaction(){
    logCSV "START" "Publish_transaction"
    cvmfs_server publish $CVMFS_REPO_NAME
    rc=$?
    if [ $rc -ne 0 ]
    then
	logCSV "FAILED" "Publish_transaction"
        exit 1          
    else
	logCSV "SUCCESS" "Publish_transaction"
    fi
}

abort_transaction(){
    logCSV "START" "Abort_transaction"
    cvmfs_server abort -f $CVMFS_REPO_NAME
    rc=$?
    if [ $rc -ne 0 ]
    then
       logCSV "FAILED" "Abort_transaction"
       exit 1
    else
       logCSV "SUCCESS" "Abort_transaction"
   fi
}


# Create the setup
#
start_transaction
lbuc_setup  --transaction_total=100 --mu=4000 --sigma=10 --period=7 --transactions_per_period=2200 $INSTALLROOT
publish_transaction


# Perform a day of operations
rc=0
while true
do
    while [ $rc -eq 0 ]
    do
        start_transaction
        if [ $rc -ne 0 ]
        then
            continue
        fi
        logCSV "START" "lbuc_install"
        NBYTES=`lbuc_install ${INSTALLROOT}`
        echo "NBYTES: $NBYTES"
        rc=$?
        if [ $rc -ne 0 ]
        then
            # Even if return code is 1, this only means that the generation
            # for today  ended
            logCSV "SUCCESS" "lbuc_install" "$NBYTES"
            continue
        fi
        logCSV "SUCCESS" "lbuc_install" "$NBYTES"

        publish_transaction
    done

    abort_transaction

    # Garbage collect
    start_transaction

    logCSV "START" "lbuc_gc"
    lbuc_gc ${INSTALLROOT}
    if [ $rc -ne 0 ]
    then
        logCSV "FAILED" "lbuc_gc"
        abort_transaction
    else
        logCSV "SUCCESS" "lbuc_gc"
        publish_transaction
    fi

    logCSV "START" "CVMFS_GC"
    CVMFS_SERVER_FLAGS=-+stats cvmfs_server gc -t '36 hours ago' -f $CVMFS_REPO_NAME
    rc=$?
    if [ $rc -ne 0 ]
    then
        logCSV "FAILED" "CVMFS_GC"

    else
        logCSV "SUCCESS" "CVMFS_GC"
    fi

    # Go to next day
    start_transaction

    logCSV "START" "lbuc_next"
    lbuc_next ${INSTALLROOT}
    if [ $rc -ne 0 ]
    then
        logCSV "FAILED" "lbuc_next"
        abort_transaction
    else
        logCSV "SUCCESS" "lbuc_next"
        publish_transaction
    fi
done

