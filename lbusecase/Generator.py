#!/usr/bin/env python
"""
Simple tool to generate files with random content

"""
from __future__ import print_function
from timeit import default_timer as timer
import argparse
import logging
import os
import random
import uuid

KiB = 1024
MiB = KiB * KiB


def generate_file(target, mu=4*KiB, sigma=500):
    # Generating filename at random
    filename = str(uuid.uuid4())
    dirname = os.path.join(target, filename[0])

    # Preparing the file
    if not os.path.isdir(dirname):
        os.makedirs(dirname)
    fullname = os.path.join(dirname, filename)
    filesize = int(random.normalvariate(mu, sigma))
    with open(fullname, "wb") as fileout:
        with open("/dev/urandom", "rb") as filein:
            fileout.write(filein.read(filesize))
    return filesize


def generate(target, transaction_size=500*MiB, mu=4*KiB, sigma=500):
    total_size = 0
    while (total_size < transaction_size):
        total_size += generate_file(target, mu, sigma)
    return total_size

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--total", help="Total size of the transaction to be created in MiB",
                        type=int, default=1024)
    parser.add_argument("--mu", help="Average size of the file to be created in bytes",
                        type=int, default=4*KiB)
    parser.add_argument("--sigma", help="Standard deviation of the size of the files to be created in bytes",
                        type=int, default=500)
    parser.add_argument("target", help="Target directory",
                        type=str, default=None)
    args = parser.parse_args()
    apath = os.path.abspath(args.target)
    logging.info("Generating total of %s MiB of files of size %s bytes (sigma: %s) in %s",
                 args.total, args.mu, args.sigma, apath)
    start = timer()
    generate(os.path.abspath(args.target),
             args.total * MiB, args.mu, args.sigma)
    end = timer()
    logging.info("Generation done in %s s", end - start)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
