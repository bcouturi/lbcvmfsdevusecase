#!/usr/bin/env python
"""
Simulate installations for a day for several slots

The way to do it
 - lbuc_setup <mypath> <period i.e. number of day to keep> <transactions per period>
    See other options for transaction size 
Then in a loop:
 - lbuc_install <mypath>
 - lbuc_gc <mypath>
 - lbuc_next <mypath>
"""
from __future__ import print_function
import argparse
import json
import logging
import os

from lbusecase.Generator import KiB, MiB
from lbusecase.PeriodMgr import install_period, cleanup


def setupdir():
    parser = argparse.ArgumentParser()
    parser.add_argument("--transaction_total", help="Total size of each transaction to be created in MiB for a given period",
                        type=int, default=1024)
    parser.add_argument("--mu", help="Average size of the file to be created in bytes",
                        type=int, default=4*KiB)
    parser.add_argument("--sigma", help="Standard deviation of the size of the files to be created in bytes",
                        type=int, default=500)
    parser.add_argument("--period", help="Number of periods after which we to garbage collect",
                        type=int)
    parser.add_argument("--transactions_per_period", help="Number of transactions per period",
                        type=int)
    parser.add_argument("target", help="Target directory",
                        type=str, default=None)
    args = parser.parse_args()
    logging.basicConfig(level=logging.INFO)
    apath = os.path.abspath(args.target)
    logging.info("Setting up config for of %s x %s MiB of files/day of size %s bytes (sigma: %s) in %s",
                 args.transactions_per_period, args.transaction_total, args.mu, args.sigma, apath)
    config = {}
    config["transactions_per_period"] = args.transactions_per_period
    config["transaction_total"] = args.transaction_total
    config["period"] = args.period
    config["mu"] = args.mu
    config["sigma"] = args.sigma

    try:
        os.makedirs(apath)
    except:
        logging.error("%s already exists, cannot setup again" % apath)
        return

    configfile = os.path.join(apath, "config.json")
    with open(configfile, "w") as f:
        json.dump(config, f)

    state = {}
    state["date"] = 1
    statefile = os.path.join(apath, "state.json")
    with open(statefile, "w") as f:
        json.dump(state, f)


def _load_metadata(apath):
    configfile = os.path.join(apath, "config.json")
    with open(configfile) as f:
        config = json.load(f)

    statefile = os.path.join(apath, "state.json")
    with open(statefile) as f:
        state = json.load(f)
    return (config, state)


def _save_state(apath, state):
    statefile = os.path.join(apath, "state.json")
    with open(statefile, "w") as f:
        json.dump(state, f)


def _get_path():
    parser = argparse.ArgumentParser()
    parser.add_argument("target", help="Target directory",
                        type=str, default=None)
    args = parser.parse_args()
    apath = os.path.abspath(args.target)
    logging.basicConfig(
        level=logging.INFO,
        #format='[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s %(message)s',)
        format='[%(asctime)s] %(levelname)s %(message)s',)
    return apath


def next():
    """ Moves to the next day """
    apath = _get_path()
    (_, state) = _load_metadata(apath)
    date = int(state["date"])
    logging.info("next %s current date:%d" % (apath, date))

    # We create a new state to remove the other flags
    newstate = {}
    newstate["date"] = date + 1

    # Saving to the json file...
    _save_state(apath, newstate)


def install():
    """ Performs the installations for the day """
    apath = _get_path()
    (config, state) = _load_metadata(apath)
    date = int(state["date"])
    logging.info("install %s date:%d" % (apath, date))

    install_done = state.get("install_done", None)
    if install_done is not None:
        logging.error("Install already done for %d" % date)
        exit(1)

    install_index = int(state.get("install_count", 0)) + 1

    if install_index > config["transactions_per_period"]:
        # Saving to the json file...
        state["install_done"] = "ok"
        _save_state(apath, state)
        exit(1)
    else:
        total_size = install_period(apath, date, install_index, config["transaction_total"] * MiB,
                                    config["mu"], config["sigma"])
        state["install_count"] = install_index
        _save_state(apath, state)
        print(total_size)


def gc():
    """ Performs the installations for the day """
    apath = _get_path()
    (config, state) = _load_metadata(apath)
    date = int(state["date"])
    logging.info("gc %s date:%d" % (apath, date))

    install_done = state.get("gc_done", None)
    if install_done is not None:
        logging.error("gc already done for %d" % date)
        return

    period = config["period"]
    cleanup(apath, date, period)

    # Saving to the json file...
    state["gc_done"] = "ok"
    _save_state(apath, state)
